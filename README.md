# Desafio React Native - ioasys

Desafio realizado por Robson Machczew.

![N|Solid](screens.png)

---

### Detalhes ###
O aplicativo foi implementado com objetivo de realizar o acesso restrito aos empreendimento cadastrados e seus detalhes, utilizando **Expo** para facilitar na visualiazação, combinação de cores dispostos no site da Ioasys, **Axios** para requisições API e componentização de alguns componentes. Também foi utilizado **Typescript** e o básico de **Redux**.
<br/>
- Como executar:
Realizar o "clone" do projeto, entrar com "yarn start" no terminal, instalar o aplicativo Expo no celular e ler o QRCode disponibilizado na página do Expo aberta pelo yarn start.


## Ferramentas
- NodeJS: v12.18.0
- React-Native: v0.63.2
- Expo: v3.28.0
 

## Libs
### Navigation (https://reactnavigation.org/):
```bash
$ npm i @react-navigation/native
$ expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view 
$ npm i @react-navigation/stack
```

### Async-Storage (https://github.com/react-native-community/async-storage):
```bash
$ npm i @react-native-community/async-storage
```

### Axios (https://github.com/axios/axios):
```bash
$ npm i axios
```

### Redux (https://redux.js.org/):
```bash
$ npm i redux react-redux
$ npm i @types/redux @types/react-redux
```

### Image-Picker (https://docs.expo.io/versions/latest/sdk/imagepicker/)
```bash
$ npm i expo-image-picker
```

### Gradient (https://docs.expo.io/versions/latest/sdk/linear-gradient/)
```bash
$ expo install expo-linear-gradient
```
