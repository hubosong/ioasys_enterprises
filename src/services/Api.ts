import axios from 'axios'

import { Alert } from 'react-native'
import { getUserToken } from '../Utils'

const api = axios.create({
    baseURL: 'https://empresas.ioasys.com.br/api/v1/',
})

api.interceptors.request.use(
    config =>
        getUserToken()
            .then((userToken: any) => {
                userToken = JSON.parse(userToken)

                config.headers['Content-Type'] = 'application/json'
                config.headers['access-token'] = userToken['access-token']
                config.headers['client'] = userToken['client']
                config.headers['uid'] = userToken['uid']

                return Promise.resolve(config)
            })
            .catch(error => {
                return Promise.resolve(config)
            }),

    error => { return Promise.reject(error) },
)

api.interceptors.response.use(
    response => {
        return response
    },
    error => {
        if ( error.request._hasError === true && error.request._response.includes('connect') ) {
            Alert.alert('Oops','Sem conexão a internet.')            
        }

        return Promise.reject(error)
    }
)

export default api