import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack'

const { Navigator, Screen } = createStackNavigator()

import AuthLoading from './pages/AuthLoading'
import Login from './pages/Login'
import Home from './pages/Home'
import Details from './pages/Details'
import Header from './components/Header'

export default function Routes(){
    return(
        <NavigationContainer>
            <Navigator initialRouteName='AuthLoading' screenOptions={{ headerShown: false, gestureEnabled: true, gestureDirection: 'horizontal', cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,}}>
                <Screen 
                    name='AuthLoading' 
                    component={AuthLoading} 
                />
                <Screen 
                    name='Login' 
                    component={Login} 
                />
                <Screen 
                    name='Home' 
                    component={Home} 
                    options={{
                        headerShown: true,
                        header: () => <Header showLogout={true} /> 
                    }} 
                />
                <Screen 
                    name='Details' 
                    component={Details} 
                    options={{ 
                        headerShown: true,
                        header: () => <Header showLogout={false} /> 
                    }} 
                />
            </Navigator>
        </NavigationContainer>
    )
}