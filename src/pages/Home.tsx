import React, { useState, useCallback, useMemo } from 'react'
import { View, Image, KeyboardAvoidingView, Alert, FlatList, Text, TouchableOpacity, ActivityIndicator, StyleSheet } from 'react-native'

import { useNavigation, useFocusEffect, StackActions } from '@react-navigation/native'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'
import { LinearGradient } from 'expo-linear-gradient'

import { globalStyles, colors } from '../styles/global_styles'
import { _rectButton, _touchableOpacity } from '../components/Buttons'
import { _textInput } from '../components/Input'
import api from '../services/Api'

interface EnterpriseData {
  id: number
  enterprise_name: string
  city: string
  country: string
  photo: string
}

/** used to render list of enterprises */
const Item = ({ item }: any) => {

  const navigation = useNavigation()

  const urlPhoto = 'https://empresas.ioasys.com.br'

  /** used to call details page with your id */
  function handleEnterpriseDetails(id: number) {
    navigation.navigate('Details', { id })
  }

  return (
    <View style={styles.listItem}>
      <Image key={item.photo} source={{ uri: urlPhoto + item.photo }} style={{ width: 60, height: 60, borderRadius: 15 }} />
      <View style={{ alignItems: "center", flex: 1, paddingTop: 10 }}>
        <Text style={{ fontWeight: "bold" }}>{item.enterprise_name}</Text>
        <Text>{item.city} / {item.country}</Text>
      </View>
      <TouchableOpacity onPress={() => handleEnterpriseDetails(item.id)} style={{ height: 50, width: 50, justifyContent: "center", alignItems: "center" }} >
        <Icon name='arrow-right' size={28} style={{ color: colors.gray, paddingTop: 10 }} />
      </TouchableOpacity>
    </View>
  )
}

export default function Home() {

  const [enterprises, setEnterprises] = useState<EnterpriseData[]>([])
  const [loading, setLoading] = useState(true)
  const [textSearch, setTextSearch] = useState('')
  const [search, setSearch] = useState('')

  const navigation = useNavigation()

  /** start with enterprises data, using useFocusEffect from navigation to auto render with goBack to page */
  useFocusEffect(() => {
    _getEnterprises()
  })
  
  /** used useCallback to save function and run only when there are changes */
  const _getEnterprises = useCallback( async () => {
    await api.get('enterprises')
      .then(response => {
        const res = response.data.enterprises
        setEnterprises(res)
        setLoading(false)
      })
      .catch(error => {
        if (error.response.status == 401) {
          Alert.alert('Aviso:', 'Não foi possível conectar aos nossos servidores.',
            [{
              text: 'Voltar para tela de login',
              onPress: () => navigation.dispatch(StackActions.replace('Login'))
            }]
          )
        }
      })
  }, [])


  /** functions used to search enterprises by name */
  function handleTextSearch(query: string) {
    setTextSearch(query)
    if(query != '') {
      handleSearch()
    } else {
      setSearch('')
    }
  }
  const handleSearch = () => {
    setSearch(textSearch)    
  }
  const filteredEnterprises = useMemo (
    () => enterprises.filter(item => item.enterprise_name.toLowerCase().includes(search.toLowerCase()) ),
    [enterprises, search]
  )

  function _clearEnterpriseMemo (){
    setSearch('')
  }
  

  return (
    <LinearGradient
      colors={[colors.purple, colors.pink]}
      style={globalStyles.container}
      start={{ x: -1, y: 0 }}
      end={{ x: 1, y: 0 }}>

      <View style={styles.container}>

        <View style={styles.inputsAllContainer}>
          <KeyboardAvoidingView style={globalStyles.inputContainer} behavior={"padding"}>
            <_textInput
              iconName='receipt'
              iconSize={28}
              iconStyle={globalStyles.inputIcon}
              placeholder="Buscar por nome"
              placeholderTextColor={colors.inputsPlaceHolder}
              value={textSearch}
              keyboardType='default'
              onChangeText={(text: string) => handleTextSearch(text)}
              style={globalStyles.input} />
          </KeyboardAvoidingView>

        </View>

        {loading
          ? (
            <ActivityIndicator animating={loading} color={colors.white} size="large" />
          )
          : (
            <FlatList
              style={{ flex: 1 }}
              data={filteredEnterprises}
              keyExtractor={(item, index) => 'key' + index}
              renderItem={({ item }) => <Item item={item} />}
            />
          )
        }

      </View>
    </LinearGradient>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%'
  },
  listItem: {
    marginTop: 10,
    padding: 10,
    backgroundColor: "#FFF",
    flex: 1,
    alignSelf: "center",
    flexDirection: "row",
    borderRadius: 12
  },
  inputsAllContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 20
  },

})