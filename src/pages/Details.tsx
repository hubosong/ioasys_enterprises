import React, { useState, useCallback } from 'react'
import { View, Image, Text, StyleSheet, ScrollView, ActivityIndicator } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { useRoute, useFocusEffect } from '@react-navigation/native'

import { globalStyles, colors } from '../styles/global_styles'
import api from '../services/Api'

interface EnterpriseData {
    id: number
    enterprise_name: string
    city: string
    country: string
    description: string
    photo: string
}

interface EnterpriseDetailsRouteParams {
    id: number
}

export default function Home() {

    /** used to get route params */
    const route = useRoute()
    const params = route.params as EnterpriseDetailsRouteParams

    const [enterprise, setEnterprise] = useState<EnterpriseData>()
    const [loading, setLoading] = useState(true)

    const url = 'https://empresas.ioasys.com.br'

    /** used to get enterprise details by id */
    useFocusEffect(() => {
        _getEnterprise()
    })

    const _getEnterprise = useCallback( async () => {
        await api.get(`enterprises/${params.id}`).then(response => {
            const res = response.data.enterprise
            setEnterprise(res)
            setLoading(false)
        })
      }, [])

    return (
        <LinearGradient
            colors={[colors.purple, colors.pink]}
            style={globalStyles.container}
            start={{ x: -1, y: 0 }}
            end={{ x: 1, y: 0 }}>

            { loading 
            ? (
                <ActivityIndicator animating={loading} color={colors.white} size="large" />
              ) 
            : (
                <ScrollView style={styles.container}>
                <View style={styles.listItem}>
                    <View style={{ alignItems: 'center', flex: 1, paddingTop: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20 }}>{enterprise?.enterprise_name}</Text>
                        <Text style={{ fontWeight: '500', fontSize: 18 }}>{enterprise?.city} / {enterprise?.country}</Text>
                        <Text style={{ fontSize: 15, marginTop: 20, textAlign: 'justify', lineHeight: 22, }}>
                            {enterprise?.description}
                        </Text>
                    </View>
                    <Image source={{ uri: url + enterprise?.photo }} style={{ width: '100%', height: 200, borderRadius: 15, marginTop: 25 }} resizeMode='contain' />
                </View>
            </ScrollView>
              ) 
          } 

        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%'
    },
    listItem: {
        marginTop: 10,
        padding: 20,
        backgroundColor: colors.white,
        flex: 1,
        alignSelf: "center",
        borderRadius: 12,
        width: '100%',
        height: '100%'
    },
})