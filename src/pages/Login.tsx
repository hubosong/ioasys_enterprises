import React, { useEffect, useCallback } from 'react'
import { View, StyleSheet, StatusBar, Text, Image, TouchableOpacity, KeyboardAvoidingView, Keyboard } from 'react-native'

import { useNavigation, StackActions } from '@react-navigation/native'
import AsyncStorage from '@react-native-community/async-storage'
import { MaterialIcons as Icon } from '@expo/vector-icons'
import { LinearGradient } from 'expo-linear-gradient'

import { globalStyles, colors } from '../styles/global_styles'
import { _rectButton, _touchableOpacity } from '../components/Buttons'
import { _textInput } from '../components/Input'
import logo from '../assets/images/logo_ioasys.png'
import Api from '../services/Api'

import { useSelector, useDispatch } from 'react-redux'
import { addUser, showPass, errMsg, hiddenComm } from '../stores/Actions'

interface RootStates {
  email: string
  password: string
  show: boolean
  hidden: boolean
  errorMessage: string
  hiddenComments: boolean
}

export default function Login() {

  /** redux states */
  const { email, password, show, hidden, errorMessage, hiddenComments } = useSelector((state: RootStates) => state)
  const dispatch = useDispatch()

  /** navigation */
  const navigation = useNavigation()

  /** used to prevent footer over textinput */
  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => dispatch(hiddenComm(true)))
    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => dispatch(hiddenComm(false)))
    return () => { keyboardDidHideListener.remove(), keyboardDidShowListener.remove() }
  }, [])

  /** used to show or hide password */
  function handleShowHiddenPass() {
    show
      ? dispatch(showPass(false, true))
      : dispatch(showPass(true, false))
  }

  /** used to register login with API call */
  async function handleSignIn() {
    if (email.length === 0 || password.length === 0) return dispatch(errMsg('Preencha usuário e senha para continuar!'))

    const credentials = { email: email, password: password }

    await Api.post('users/auth/sign_in', credentials).then(response => {
      const headers = response.headers
      const accessToken = headers['access-token']
      if (accessToken != null) {
        _storeData(headers, credentials)
        navigation.dispatch(StackActions.replace('Home'))
      }
    })    
  }

  /** used to store data with two items: headers and credentials */
  async function _storeData(headers: any, credentials: any) {
    try {
      await AsyncStorage.setItem('@userToken', JSON.stringify(headers))
      await AsyncStorage.setItem('@userData', JSON.stringify(credentials))
    } catch (error) { console.error(error) }
  }

  return (
    <>
    <StatusBar barStyle="light-content" translucent backgroundColor='transparent' />
    <LinearGradient
      colors={[colors.purple, colors.pink]}
      style={globalStyles.container}
      start={{ x: -1, y: 0 }}
      end={{ x: 1, y: 0 }}>

      <View style={styles.header}>
        <Image source={logo} resizeMode="contain" />
      </View>

      <View style={styles.body}>
        <KeyboardAvoidingView style={globalStyles.inputContainer} behavior={"padding"}>
          <_textInput
            iconName='person-outline'
            iconSize={28}
            iconStyle={globalStyles.inputIcon}
            placeholder='Digite o seu email'
            placeholderTextColor={colors.inputsPlaceHolder}
            value={email}
            onChangeText={(text: string) => dispatch(addUser(text, password))}
            keyboardType='email-address'
            returnKeyType='next'
            style={globalStyles.input} />
        </KeyboardAvoidingView>

        <KeyboardAvoidingView style={globalStyles.inputContainer}>
          <_textInput
            iconName='lock-outline'
            iconSize={28}
            iconStyle={globalStyles.inputIcon}
            placeholder="Digite a sua senha"
            placeholderTextColor={colors.inputsPlaceHolder}
            value={password}
            onChangeText={(text: string) => dispatch(addUser(email, text))}
            secureTextEntry={show}
            keyboardType='numeric'
            returnKeyType='go'
            onSubmitEditing={useCallback(handleSignIn, [])}
            style={globalStyles.input} />

          <TouchableOpacity onPress={handleShowHiddenPass} style={styles.buttonPass}>
            <Icon
              name={hidden == false ? 'visibility' : 'visibility-off'}
              size={28} color={colors.purplepink}
            />
          </TouchableOpacity>
        </KeyboardAvoidingView>

        {errorMessage.length !== 0 && <Text style={styles.errorMessage}>{errorMessage}</Text>}

        <_rectButton
          onPress={useCallback(handleSignIn, [])}
          style={{ ...globalStyles.button, margin: 15 }}
          textStyle={globalStyles.buttonText}
          buttonText='E N T R A R' />

        <_touchableOpacity
          onPress={useCallback(() => alert('@'), [])}
          style={{ alignSelf: 'flex-start', paddingStart: 30, }}
          textStyle={{ color: colors.white, textAlign: 'center' }}
          buttonText='Esqueceu sua senha?' />

      </View>

      <View style={styles.footer}>
        <View style={[styles.showView, hiddenComments ? styles.hiddenView : {}]}>
          <_touchableOpacity
            onPress={useCallback(() => alert('@'), [])}
            style={{ marginTop: 20 }}
            textStyle={{ color: colors.white, textAlign: 'center' }}
            buttonText='© 2020 胡博嵩' />
        </View>
      </View>

    </LinearGradient>
    </>
  )
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '45%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  body: {
    width: '100%',
    height: '45%'
  },
  footer: {
    width: '100%',
    height: '10%',
    paddingHorizontal: 20,
    justifyContent: 'center'
  },
  buttonPass: {
    position: 'absolute',
    top: 8,
    right: 37,
    zIndex: 3,
  },
  errorMessage: {
    textAlign: 'center',
    color: colors.error,
    fontSize: 14,
    marginBottom: 10,
    marginTop: 15,
    marginHorizontal: 20,
  },
  showView: {
    width: '100%',
    height: 'auto'
  },
  hiddenView: {
    width: 0,
    height: 0
  }
})