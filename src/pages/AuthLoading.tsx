import React, { useEffect } from 'react'
import { View, ActivityIndicator } from 'react-native'
import { useNavigation, StackActions } from '@react-navigation/native'
import AsyncStorage from '@react-native-community/async-storage'
import { LinearGradient } from 'expo-linear-gradient'

import { globalStyles, colors } from '../styles/global_styles'

export default function AuthLoading() {

    const navigation = useNavigation()

    useEffect(() => {
        async function handleUserNextScreen() {
            const userToken = await AsyncStorage.getItem('@userToken')
            navigation.dispatch(StackActions.replace(userToken ? 'Home' : 'Login'))
        }
        handleUserNextScreen()
    }, [])

    return (
        <LinearGradient
            colors={[colors.purple, colors.pink]}
            style={globalStyles.container}
            start={{ x: -1, y: 0 }}
            end={{ x: 1, y: 0 }}>

            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" color={colors.white} />
            </View>

        </LinearGradient>

    )

}