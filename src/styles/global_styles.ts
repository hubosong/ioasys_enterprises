import { StyleSheet } from 'react-native'

export const colors = {
    purple: '#3c1e81',
    pink: '#bf218d',
    purplepink: '#9f258d',

    buttonsTextColor: '#ffffff',
    inputs: '#f7f7f7',
    inputBorder: '#888',
    error: '#ce2029',
    inputsPlaceHolder: '#888',

    gray: '#292d30',
    darkgray: '#151618',
    white: '#ffffff'
    
}

export const globalStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.darkgray,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 12
      },
      button: {
        paddingVertical: 12,
        backgroundColor: colors.purple,
        alignSelf: 'stretch',
        marginHorizontal: 25,
        borderRadius: 12,
        borderWidth: 3,
      },
      buttonText: {
        color: colors.buttonsTextColor,
        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center',
      },

      inputContainer: {
        flexDirection: 'row',
      },
      inputIcon: {
        position: 'absolute',
        top: 10,
        left: 35,
        zIndex: 2,
        color: colors.purplepink
      },
      input: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 8,
        backgroundColor: colors.inputs,
        zIndex: 1,
        alignSelf: 'stretch',
        marginBottom: 15,
        marginHorizontal: 25,
        paddingStart: 45,
        fontSize: 16,
        fontWeight: 'bold',
        color: colors.purple,
        borderRadius: 12,
        borderWidth: 2,
        borderColor: colors.inputBorder
      },
})
