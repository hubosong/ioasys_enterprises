import React from 'react'
import { Text, TouchableOpacity, Image } from 'react-native'

/** lib with ripple button */
import { RectButton } from 'react-native-gesture-handler'
import { colors } from '../styles/global_styles'

export function _rectButton(props: any) {
    return (
        <RectButton
            onPress={props.onPress}
            style={props.style}
            rippleColor={colors.purplepink}>
            <Text style={props.textStyle}>{props.buttonText}</Text>
        </RectButton>
    )
}

export function _touchableOpacity(props: any) {
    return (
        <TouchableOpacity style={props.style} onPress={props.onPress}>
            <Text style={props.textStyle}>{props.buttonText}</Text>
        </TouchableOpacity>

    )
}