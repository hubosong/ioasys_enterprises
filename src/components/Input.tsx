import React from 'react'
import { TextInput } from 'react-native'
import { MaterialIcons as Icon } from '@expo/vector-icons'

export function _textInput(props: any) {
    return (
        <>
        <Icon name={props.iconName} size={props.iconSize} style={props.iconStyle} />
        <TextInput
            placeholder={props.placeholder}
            placeholderTextColor={props.placeholderTextColor}
            value={props.value}
            onChangeText={props.onChangeText}
            secureTextEntry={props.secureTextEntry}
            autoCapitalize="none"
            autoCorrect={false}
            maxLength={props.maxLength}
            underlineColorAndroid='transparent'
            keyboardType={props.keyboardType}
            returnKeyType={props.returnKeyType}
            onSubmitEditing={props.onSubmitEditing}
            blurOnSubmit={false}
            style={props.style} />
        </>
    )
}