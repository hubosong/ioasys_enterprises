import React from 'react'
import { View, StatusBar, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { useNavigation, StackActions } from '@react-navigation/native'
import { LinearGradient } from 'expo-linear-gradient'
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons'

import logo from '../assets/images/ioasys.png'
import { colors } from '../styles/global_styles'
import { deleteUserToken } from '../Utils'

interface HeaderProps {
    showLogout?: boolean
}

export default function Header ({ showLogout = true }: HeaderProps) {

    const navigation = useNavigation()

    /** used to clear token from storage */
    async function handleClearToken() {
        await deleteUserToken()
        navigation.dispatch(StackActions.replace('Login'))
        console.log("token clear")
    }

    return (
        <>
        <StatusBar barStyle="light-content" translucent backgroundColor='transparent' />
        <LinearGradient
            colors={[colors.purple, colors.pink]}
            style={styles.headerContainer}
            start={{ x: -1, y: 0 }}
            end={{ x: 1, y: 0 }}>

            <View style={{ width: '50%', left: 0, alignItems: 'flex-start' }}>
                <Image source={logo} resizeMode="contain" style={{ width: '100%', height: 50 }} />
            </View>

            { showLogout ? (
                <View style={{ width: '50%', right: 0, alignItems: 'flex-end' }}>
                    <TouchableOpacity onPress={handleClearToken} >
                        <Icon name='logout' size={30} style={{color: colors.white}} />
                    </TouchableOpacity>
                </View>
            ) : (
                <View style={{ width: '50%', right: 0, alignItems: 'flex-end' }}>
                    <TouchableOpacity onPress={navigation.goBack} >
                        <Icon name='arrow-left' size={30} style={{color: colors.white}} />
                    </TouchableOpacity>
                </View>
            ) }

        </LinearGradient>
        </>
    )
}

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

        padding: 24,
        borderBottomWidth: 3,
        borderColor: colors.pink,
        paddingTop: 50,
    },
    title: {
        color: colors.white,
        fontSize: 16,
    }
})
