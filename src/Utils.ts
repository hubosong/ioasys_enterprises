import AsyncStorage from '@react-native-community/async-storage'

export async function getUserToken() {
  try {
    return await AsyncStorage.getItem('@userToken');
  } catch (e) {
    throw e
  }
}

export async function deleteUserToken() {
  try {
    return await AsyncStorage.removeItem('@userToken');
  } catch (e) {
    throw e;
  }
}

export async function getUserData() {
  try {
    return await AsyncStorage.getItem('@userData');
  } catch (e) {
    throw e
  }
}

export async function deleteUserData() {
  try {
    return await AsyncStorage.removeItem('@userData');
  } catch (e) {
    throw e;
  }
}